/*console.log(`hello world`)*/

// fetch()
	// this is a method in JavaScipt that is used to send request in the sever and load the information  (response from the server) in the webpages.

	// Syntax:
		// fetch("urlAPI", {options})
			// url - the url which the request is to be made.
			// options - an array of properties, optional parameter.

/*Endpoint of the API
GET and POST:
https://sheltered-hamlet-48007.herokuapp.com/movies

Update and Delete:https://sheltered-hamlet-48007.herokuapp.com/movies/:id*/

// Get all data

let fetchMovies = ()=> {
	fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
	.then(response=> response.json())
	.then(data=> showPosts(data))
}

fetchMovies();

// Show fetch data in the document (HTML web page)

const showPosts = (movies) => {
	console.log(movies)
	// Create a variable that will contain all the movies.
	let movieEntries = "";

	movies.forEach((movie)=> {
		// onclick() is an event occurs when the user clock on an element.
			// This allows us to execute a JavaScrits's wheb\n an element get clicked
		// We can assign HTML elements in JS variables

		movieEntries += `
			<div id = "movie-${movie._id}">
			
			<h3 id ="movie-title-${movie._id}">${movie.title}</h3>
			
			<p id = "movie-desc-${movie._id}">${movie.description}</p>

			<button onclick = "editMovie('${movie._id}')">Edit</button>
			
			<button onclick = "deleteMovie('${movie._id}')">Delete</button>
			</div>`;
	});

	/*console.log(movieEntries)*/
	document.querySelector("#div-movie-entries").innerHTML =movieEntries

}

// Add movie data
document.querySelector("#form-add-movie").addEventListener("submit", (event)=>{
	// prevents the page from loading
	event.preventDefault();

	let title = document.querySelector("#txt-title").value;
	let description = document.querySelector("#txt-desc").value;

	fetch("https://sheltered-hamlet-48007.herokuapp.com/movies", {method: "POST",
		body: JSON.stringify({
			title: title,
			description: description
		}),
		headers:{
			"Content-Type": "application/json"
		}
	}).then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Successfully added.")
	})


	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-desc").value = null;

	// to show new movies;
	fetchMovies();

	// 
})

// Edit movie button
// Create the JS function called in the onclick event.

const editMovie = (id)=>{
	console.log(id);
	let title = document.querySelector(`#movie-title-${id}`).innerHTML;
	let description = document.querySelector(`#movie-desc-${id}`).innerHTML;
	// console.log(title);
	// console.log(description);

	// Pass the id, title, and description in the Edit form and enable the Update button.

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-desc").value = description;
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// Update a movie
// This will trigger an event that will update a certain movie upon clicking the update

document.querySelector("#form-edit-movie").addEventListener("submit", (event) =>{
	
	event.preventDefault();

	let id = document.querySelector("#txt-edit-id").value;

	fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
		method: "PUT",
		body: JSON.stringify({
			title: document.querySelector("#txt-edit-title").value,
			description: document.querySelector("#txt-edit-desc").value
		}),
		headers:{
			"Content-Type": "application/json"
		}
	})
	.then(response => response.json()).then(data => {
		console.log(data);
		alert("Successfully updated.")
		document.querySelector("#txt-edit-id").value = null;
	document.querySelector("#txt-edit-title").value=null;
	document.querySelector("#txt-edit-desc").value = null;
	document.querySelector("#btn-submit-update").setAttribute("disabled", true);


	// show updates
	fetchMovies();
	})

	
})

const deleteMovie = (id) => {
	console.log(id)
	fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {method: "DELETE"})
	.then(response => {alert("Successfully deleted!")
fetchMovies();}
		)

	
}

